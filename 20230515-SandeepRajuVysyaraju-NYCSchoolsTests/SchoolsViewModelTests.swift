//
//  SchoolsViewModelTests.swift
//  20230515-SandeepRajuVysyaraju-NYCSchoolsTests
//
//  Created by SandeepRaju Vysyaraju  on 16/05/23.
//

import XCTest
@testable import _0230515_SandeepRajuVysyaraju_NYCSchools

final class SchoolsViewModelTests: XCTestCase {
    
    var sut: SchoolsViewModel!
    var mockservice: MockSchoolsViewService!
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        mockservice = MockSchoolsViewService()
        sut = SchoolsViewModel(schoolsViewService: mockservice)
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        mockservice = nil
        sut = nil
    }


    func testSchoolsAPI_Failure() {
        mockservice.schoolsResult = .failure(.noData)
        sut.fetchSchools()
        XCTAssert(sut.filteredSchools.isEmpty)
    }
    
    func testSchoolsAPI_Success() {
        guard let schools = mockservice.getSchools() else { return }
        mockservice.schoolsResult = .success(schools)
        sut.fetchSchools()
        XCTAssert(sut.filteredSchools.count == 5)
    }
    
    func testSchoolsSATScoreAPI_Failure() {
        guard let schools = mockservice.getSchools() else { return }
        sut.selectedSchool = schools.filter{$0.dbn == "08X282"}.first
        mockservice.schoolsSATResult = .failure(.noData)
        sut.fetchSchoolSATs()
        XCTAssertNil(sut.selectedSchootSATScores)
    }
    
    func testSchoolsSATScoreAPI_Success() {
        guard let schoolsSAT = mockservice.getSchoolSATScores(for: "08X282" ) else { return }
        mockservice.schoolsSATResult = .success(schoolsSAT)
        guard let schools = mockservice.getSchools() else { return }
        sut.selectedSchool = schools.filter{$0.dbn == "08X282"}.first
        sut.fetchSchoolSATs()
        XCTAssertNotNil(sut.selectedSchootSATScores)
    }
    
    /// Test to check all school information
    func testSelectedSchoolInfromation() {
        guard let schools = mockservice.getSchools() else { return }
        sut.filteredSchools = schools
        sut.setSelectedSchool(at: IndexPath(row: 0, section: 0))
        
        //Asserting school Name
        XCTAssertFalse(sut.schoolName.isEmpty)
        
        //Asserting school address
        XCTAssertFalse(sut.schooldAddress.isEmpty)
        
        //Asserting school overview
        XCTAssertFalse(sut.schoolOverview.isEmpty)
        
        //Asserting school number of sat test takers
        XCTAssertFalse(sut.numberOfSATTestTakers.isEmpty)

        //Asserting math average score
        XCTAssertFalse(sut.mathAvgScore.isEmpty)

        //Asserting writing average score
        XCTAssertFalse(sut.writingAvgScore.isEmpty)

        //Asserting reading average score
        XCTAssertFalse(sut.readingAvgScore.isEmpty)

    }
    
    /// Test to check all school information without selected school
    func testSelectedSchoolInfromationWithoutSelectedSchool() {
        //Asserting school Name
        XCTAssertTrue(sut.schoolName.isEmpty)
        
        //Asserting school address
        XCTAssertTrue(sut.schooldAddress.isEmpty)
        
        //Asserting school overview
        XCTAssertTrue(sut.schoolOverview.isEmpty)
        
        //Asserting school number of sat test takers
        XCTAssertFalse(sut.numberOfSATTestTakers.isEmpty)

        //Asserting math average score
        XCTAssertFalse(sut.mathAvgScore.isEmpty)

        //Asserting writing average score
        XCTAssertFalse(sut.writingAvgScore.isEmpty)

        //Asserting reading average score
        XCTAssertFalse(sut.readingAvgScore.isEmpty)

    }

    
    func testPerformSearch() {
        guard let schools = mockservice.getSchools() else { return }
        sut.schools = schools
        sut.filteredSchools = schools
        sut.performSearch(with: "School")
        XCTAssertEqual(sut.filteredSchools.count, 4)
    }
    
    func testPerformSearch_Empty() {
        guard let schools = mockservice.getSchools() else { return }
        sut.schools = schools
        sut.filteredSchools = schools
        sut.performSearch(with: "")
        XCTAssertEqual(sut.filteredSchools.count, 5)
    }

    func testEmptySelectedInformation() {
        sut.clearSelectedSchoolInformation()
        XCTAssertNil(sut.selectedSchool)
        XCTAssertNil(sut.selectedSchootSATScores)
    }
}
