//
//  MockSchoolsViewService.swift
//  20230515-SandeepRajuVysyaraju-NYCSchoolsTests
//
//  Created by SandeepRaju Vysyaraju  on 16/05/23.
//

import Foundation
@testable import _0230515_SandeepRajuVysyaraju_NYCSchools

class MockSchoolsViewService: SchoolsViewServiceDelegate {
    var schoolsResult: Result<[_0230515_SandeepRajuVysyaraju_NYCSchools.SchoolModel]?, _0230515_SandeepRajuVysyaraju_NYCSchools.SchoolsError>!
    var schoolsSATResult: Result<[_0230515_SandeepRajuVysyaraju_NYCSchools.SchoolSATModel]?, _0230515_SandeepRajuVysyaraju_NYCSchools.SchoolsError>!
    
    func getSchools(completion: @escaping (Result<[_0230515_SandeepRajuVysyaraju_NYCSchools.SchoolModel]?, _0230515_SandeepRajuVysyaraju_NYCSchools.SchoolsError>) -> Void) {
        completion(schoolsResult)
    }
    
    func getSchoolSATScores(for dbn: String, completion: @escaping (Result<[_0230515_SandeepRajuVysyaraju_NYCSchools.SchoolSATModel]?, _0230515_SandeepRajuVysyaraju_NYCSchools.SchoolsError>) -> Void) {
        completion(schoolsSATResult)
    }
    
    func getSchools() -> [SchoolModel]? {
        guard let jsonData = readLocalJsonFile(resource: "Schools") else {
            return nil
        }
        do {
            let obj = try  JSONDecoder().decode([_0230515_SandeepRajuVysyaraju_NYCSchools.SchoolModel].self, from: jsonData)
            return obj
        } catch _ {
            return nil
        }
    }
    
    func getSchoolSATScores(for dbn: String) -> [SchoolSATModel]? {
        guard let jsonData = readLocalJsonFile(resource: "SchoolSATScores") else {
            return nil
        }
        do {
            let obj = try  JSONDecoder().decode([_0230515_SandeepRajuVysyaraju_NYCSchools.SchoolSATModel].self, from: jsonData)
            let schoolSATObj = obj.filter { school in
                return school.dbn == dbn
            }
            return schoolSATObj
        } catch let error {
            print(error)
            return nil
        }
    }
    
    private func readLocalJsonFile(resource name: String) -> Data? {
        do {
            guard let fileUrl =  Bundle.main.url(forResource: name, withExtension: "json") else {
                return nil
            }
            let data = try Data(contentsOf: fileUrl)
            return data
        } catch let error {
            print(error.localizedDescription)
            return nil
        }
    }
}
