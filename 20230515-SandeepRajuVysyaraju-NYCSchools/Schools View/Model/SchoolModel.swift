//
//  SchoolModel.swift
//  20230515-SandeepRajuVysyaraju-NYCSchools
//
//  Created by SandeepRaju Vysyaraju  on 15/05/23.
//

import Foundation

/// SchoolModel codable instance to parse school response
struct SchoolModel: Codable {
    var dbn: String
    var schoolName: String?
    var overviewParagraph: String?
    var school10thSeats: String?
    var location: String?
    var phoneNumber: String?
    var faxNumber: String?
    var schoolEmail: String?
    var website: String?
    var totalStudents: String?
    var primaryAddressLine1: String?
    var city: String?
    var zip: String?
    var stateCode: String?
    var latitude: String?
    var longitude: String?

    enum CodingKeys: String, CodingKey {
        case dbn
        case location
        case website
        case city
        case zip
        case latitude
        case longitude

        case schoolName = "school_name"
        case overviewParagraph = "overview_paragraph"
        case phoneNumber = "phone_number"
        case faxNumber = "fax_number"
        case schoolEmail = "school_email"
        case totalStudents = "total_students"
        case primaryAddressLine1 = "primary_address_line_1"
        case stateCode = "state_code"
    }
}
