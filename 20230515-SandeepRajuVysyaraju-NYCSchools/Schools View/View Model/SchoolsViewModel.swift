//
//  SchoolsViewModel.swift
//  20230515-SandeepRajuVysyaraju-NYCSchools
//
//  Created by SandeepRaju Vysyaraju  on 15/05/23.
//

import Foundation
import UIKit
import MapKit


/// Enum with the possible view states
enum ViewState {
    case idle
    case loading
    case success
    case satsSuccess
    case error(SchoolsError)
}


/// Delegate method to update the view state
protocol SchoolsViewModelDelegate: AnyObject {
    func didUpdateViewState(with state: ViewState)
}

/// Viewmodel class which holds all the business logic of the usecase
class SchoolsViewModel {
    
    /// viewmodels weak delegate to notify the assigned instance
    weak var delegate: SchoolsViewModelDelegate?
    
    /// service delegate to perform network calls and can be used as property injection
    private let schoolsViewService: SchoolsViewServiceDelegate
    
    /// property which holds the selected school
    var selectedSchool: SchoolModel?
    
    /// property which holds the selected school sat scores
    var selectedSchootSATScores: SchoolSATModel?
    
    /// property which holds the viewstate and performs an action when a state is set to it
    private var state: ViewState {
        didSet {
            delegate?.didUpdateViewState(with: state)
        }
    }
    
    /// Property which holds all the schools information
    var schools: [SchoolModel] = []
    
    /// Property which holds all the filtered school information
    var filteredSchools: [SchoolModel] = []
    
    /// Init method which takes the service class instance as the method injection
    /// - Parameter schoolsViewService: holds the instance of the schools service class
    init(schoolsViewService: SchoolsViewServiceDelegate = SchoolsViewService()) {
        self.state = .idle
        self.schoolsViewService = schoolsViewService
    }
    
}

//MARK: - Search Functionality
extension SchoolsViewModel {
    
    ///  Method which performs the filter based on the user input
    /// - Parameter text: need to pass the search text inorder perform the filter
    func performSearch(with text: String) {
        if text.isEmpty {
            self.filteredSchools = self.schools
        } else {
            self.filteredSchools = self.schools.filter({ school in
                guard let schoolName = school.schoolName else { return false}
                return schoolName.lowercased().contains(text.lowercased())
            })
        }
        self.state = .success
    }
}

//MARK: - DataSource
extension SchoolsViewModel {
    
    /// property which holds number of school items
    var numberOfItems: Int {
        return filteredSchools.count
    }
    
    /// method which updates the selected school
    /// - Parameter indexpath: need to pass the indexpath to pull the selected school
    func setSelectedSchool(at indexpath: IndexPath) {
        selectedSchool = filteredSchools[indexpath.row]
    }
    
    /// property which holds the school name
    var schoolName: String {
        guard let selectedSchool = selectedSchool else { return "" }
        return selectedSchool.schoolName ?? ""
    }
    
    /// property which holds the school address
    var schooldAddress: String {
        guard let selectedSchool = selectedSchool else { return "" }
        return (selectedSchool.primaryAddressLine1 ?? "") + " " + (selectedSchool.city ?? "") + " " + (selectedSchool.zip ?? "")
    }
    
    /// property which holds the school overview
    var schoolOverview: String {
        guard let selectedSchool = selectedSchool else { return "" }
        return selectedSchool.overviewParagraph ??  ""
    }
    
    /// property which holds the numberr of the SAT test takers
    var numberOfSATTestTakers: String {
        guard let selectedSchootSATScores = selectedSchootSATScores else { return "SAT Test Takers: " }
        return "SAT Test Takers: \(selectedSchootSATScores.numOfSatTestTakers)"
    }
    
    /// property which holds the maths average score
    var mathAvgScore: String {
        guard let selectedSchootSATScores = selectedSchootSATScores else { return "Math Average Score: " }
        return "Math Average Score: \(selectedSchootSATScores.satMathAvgScore)"
    }
    
    /// property which holds the writing average score
    var writingAvgScore: String {
        guard let selectedSchootSATScores = selectedSchootSATScores else { return "Writing Average Score: " }
        return "Writing Average Score: \(selectedSchootSATScores.satWritingAvgScore)"
    }
    
    /// property which holds the reading average score
    var readingAvgScore: String {
        guard let selectedSchootSATScores = selectedSchootSATScores else { return "Reading Average Score: " }
        return "Reading Average Score: \(selectedSchootSATScores.satCriticalReadingAvgScore)"
    }
    
    /// method that helps to clear the selected information
    func clearSelectedSchoolInformation() {
        self.selectedSchool = nil
        self.selectedSchootSATScores = nil
    }
}

//MARK: - Service
extension SchoolsViewModel {
    
    /// method use to fetch schools and notifies once the vc once it completes fetching
    func fetchSchools() {
        self.state = .loading
        self.schoolsViewService.getSchools { result in
            switch result {
            case .success(let schools):
                let schools = schools?.sorted{$0.schoolName! < $1.schoolName!} ?? []
                self.schools = schools
                self.filteredSchools = schools
                self.state = .success
            case .failure(let error):
                self.schools = []
                self.filteredSchools = []
                self.state = .error(error)
            }
        }
    }
    
    /// method use to fetch school SAT scores based on the dbn
    func fetchSchoolSATs() {
        guard let selectedSchoolDbn = selectedSchool?.dbn else { return }
        self.state = .loading
        self.schoolsViewService.getSchoolSATScores(for: selectedSchoolDbn, completion: { result in
            switch result {
            case .success(let schoolSATs):
                self.selectedSchootSATScores = schoolSATs?.first
                self.state = .satsSuccess
            case .failure(let error):
                self.selectedSchootSATScores = nil
                self.state = .error(error)
            }
        })
    }
}

//MARK: - School Details methods
extension SchoolsViewModel {
    
    /// method helps to perform initiating phone call
    /// - Parameter completion: parameter helps to pass the completion status back 
    func initiateCall(completion: @escaping (_ accepted: Bool) -> Void) {
        guard let phoneNumber = selectedSchool?.phoneNumber else { return }
        
        guard let phoneNumberURL = URL(string: "tel://\(phoneNumber)") else { return }
        
        if UIApplication.shared.canOpenURL(phoneNumberURL) {
            completion(true)
            UIApplication.shared.open(phoneNumberURL)
        } else {
            completion(false)
        }
    }
    
    /// method helps to perform showing website
    /// - Parameter completion: parameter helps to pass the completion status back
    func showWebsite(completion: @escaping (_ accepted: Bool) -> Void) {
        guard let website = selectedSchool?.website else { return }
        
        guard let websiteURL = URL(string: website) else { return }
        
        if UIApplication.shared.canOpenURL(websiteURL) {
            completion(true)
            UIApplication.shared.open(websiteURL)
        }else {
            completion(false)
        }
    }
    
    /// method helps to perform initiating navigation
    func initateNavigation() {
        guard let latitude = selectedSchool?.latitude, let lat = Double(latitude) else { return }
        guard let longitude = selectedSchool?.longitude, let long = Double(longitude) else { return }
        
        let coordinate = CLLocationCoordinate2DMake(lat, long)
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary: nil))
        mapItem.name = selectedSchool?.schoolName ?? ""
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
    }
}
