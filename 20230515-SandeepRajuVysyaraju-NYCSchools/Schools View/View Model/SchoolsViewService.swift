//
//  SchoolsViewService.swift
//  20230515-SandeepRajuVysyaraju-NYCSchools
//
//  Created by SandeepRaju Vysyaraju  on 15/05/23.
//

import Foundation

/// Enum cases for the school error, we can append more cases based on the requirement and would be able to handle accordingly
enum SchoolsError: Error {
    case noData
    case badURL
    case decodingError
}

/// Protocol for the school service,
protocol SchoolsViewServiceDelegate {
    func getSchools(completion: @escaping(Result<[SchoolModel]?, SchoolsError>) -> Void)
    func getSchoolSATScores(for dbn:String, completion: @escaping(Result<[SchoolSATModel]?, SchoolsError>) -> Void)
}

/// Class which handles the service calls for the school
class SchoolsViewService: SchoolsViewServiceDelegate {
    
    /// This method fetches the schools over the network
    /// - Parameter completion: completion block returns the success and failure cases with respective data
    func getSchools(completion: @escaping(Result<[SchoolModel]?, SchoolsError>) -> Void) {
        guard let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json") else {
            return completion(.failure(.badURL))
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                return completion(.failure(.noData))
            }
            
            let schoolsResponse = try? JSONDecoder().decode([SchoolModel].self, from: data)
            
            if let schoolsResponse = schoolsResponse {
                return completion(.success(schoolsResponse))
            } else {
                return completion(.failure(.decodingError))
            }
        }.resume()
    }
    
    /// This method helps to fetch the school sat scores based on the dbn input
    /// - Parameters:
    ///   - dbn: need to pass the dbn to fetch the scores of a particular school
    ///   - completion: completion block returns the success and failure cases with respective data
    func getSchoolSATScores(for dbn: String, completion: @escaping (Result<[SchoolSATModel]?, SchoolsError>) -> Void) {
        guard let url = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(dbn)") else {
            return completion(.failure(.badURL))
        }
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                return completion(.failure(.noData))
            }
            
            let schoolSATResponse = try? JSONDecoder().decode([SchoolSATModel].self, from: data)
            
            if let schoolSATResponse = schoolSATResponse {
                return completion(.success(schoolSATResponse))
            } else {
                return completion(.failure(.decodingError))
            }
        }.resume()
    }
}
