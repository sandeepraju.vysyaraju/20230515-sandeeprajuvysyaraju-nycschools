//
//  SchoolsViewController.swift
//  20230515-SandeepRajuVysyaraju-NYCSchools
//
//  Created by SandeepRaju Vysyaraju  on 15/05/23.
//

import UIKit


/// This view controller helps to show the schools in the table view, we can imporve more by adding the error cases UI. But as of now didn't add them, added the functionality to view and search schools. This screen shows the basic information of the school such as school name and school address 
class SchoolsViewController: UIViewController, SchoolsViewModelDelegate {
    
    /// Tableview to show schools data
    @IBOutlet weak var schoolsTV: UITableView!
    
    /// Search bar to search schools
    @IBOutlet weak var schoolsSearchBar: UISearchBar!
    
    /// Activity Indicator to show during network calls
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    /// viewmodel which holds the business logic
    private var viewModel: SchoolsViewModel!
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel = SchoolsViewModel()
        self.viewModel.delegate = self
        self.schoolsSearchBar.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.fetchSchools()
    }
    
    /// Starts the animation of activity indicator
    func startLoading() {
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
    }
    
    /// Stops the animation of activity indicator
    func stopLoading() {
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
    }
    
    /// Navigation method, which helps to pass the information to the destination controller, as we have only one detail vc we are not checking the segue identifier to find the right vc.
    /// - Parameters:
    ///   - segue: provides all the information of the segue such as identifier, source and destination controller
    ///   - sender: holds the sender information
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as? SchoolsDetailViewController
        vc?.viewModel = self.viewModel
    }
}

//MARK: - ViewModel Delegate
extension SchoolsViewController {
    
    /// This delegate method helps to find the view state and so we can act accordingly. We will be using main queue inorder to update the UI
    /// - Parameter state: provides the current viewstate
    func didUpdateViewState(with state: ViewState) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            switch state {
            case .idle:
                break
            case .loading:
                self.startLoading()
            case .success:
                self.schoolsTV.reloadData()
                self.stopLoading()
            case .error(_):
                self.stopLoading()
            case .satsSuccess:
                self.stopLoading()
                self.schoolsSearchBar.resignFirstResponder()
                self.performSegue(withIdentifier: "SchoolDetailViewID", sender: nil)
                self.schoolsSearchBar.text = ""
            }
        }
    }
}

//MARK: - Table View Delegate & Datasource
extension SchoolsViewController: UITableViewDelegate, UITableViewDataSource {
    
    /// Table view delegate method for number of rows
    /// - Parameters:
    ///   - tableView: holds the instance of the current visible table view
    ///   - section: hold the sections
    /// - Returns: need to pass number of items to show in a section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItems
    }
    
    /// Table view delegate method for cell creation
    /// - Parameters:
    ///   - tableView: holds the instance of the current visible table view
    ///   - indexPath: holds the index path
    /// - Returns: need to pass the desired cell to show in table view
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Reuse or create a cell.
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolCell", for: indexPath)
        viewModel.setSelectedSchool(at: indexPath)
        cell.textLabel?.text = viewModel.schoolName
        cell.detailTextLabel?.text = viewModel.schooldAddress
        return cell
    }
    
    /// Tableview delegate method which triggers when a row is selected
    /// - Parameters:
    ///   - tableView: holds the instance of the current visible table view
    ///   - indexPath: holds the indexpath
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.setSelectedSchool(at: indexPath)
        viewModel.fetchSchoolSATs()
    }
}


//MARK: - Searchbar Deleagate
extension SchoolsViewController: UISearchBarDelegate {
    
    /// Searchbar delegate method  which triggers when the search text changes
    /// - Parameters:
    ///   - searchBar: holds the instance of Search bar
    ///   - searchText: holds the user inputted text
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.performSearch(with: searchText)
    }
}
