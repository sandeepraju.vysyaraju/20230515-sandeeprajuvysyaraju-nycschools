//
//  SchoolsDetailViewController.swift
//  20230515-SandeepRajuVysyaraju-NYCSchools
//
//  Created by SandeepRaju Vysyaraju  on 16/05/23.
//

import UIKit

/// Class that shows the schools detail information
class SchoolsDetailViewController: UIViewController {
    
    /// viewmodel property
    var viewModel: SchoolsViewModel!
    
    /// all UI Outlets
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var schoolAddressLabel: UILabel!
    @IBOutlet weak var noOfSATTesttakersLabel: UILabel!
    @IBOutlet weak var mathScoreLabel: UILabel!
    @IBOutlet weak var readingScoreLabel: UILabel!
    @IBOutlet weak var writingScoreLabel: UILabel!
    @IBOutlet weak var overviewTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    deinit {
        viewModel.clearSelectedSchoolInformation()
    }
    
    /// call action method
    /// - Parameter sender: the instance from which it invoked
    @IBAction func callTapped(_ sender: Any) {
        viewModel.initiateCall { [weak self] accepted in
            if !accepted {
                self?.showAlert()
            }
        }
    }
    
    /// web action method
    /// - Parameter sender: the instance from which it invoked
    @IBAction func webTapped(_ sender: Any) {
        viewModel.showWebsite { [weak self] accepted in
            if !accepted {
                self?.showAlert()
            }
        }
    }
    
    /// navigation action method
    /// - Parameter sender: the instance from which it invoked
    @IBAction func directionsTapped(_ sender: Any) {
        viewModel.initateNavigation()
    }
}

extension SchoolsDetailViewController {
    
    /// method to update the UI content
    func updateUI() {
        self.schoolNameLabel.text = viewModel.schoolName
        self.schoolAddressLabel.text = viewModel.schooldAddress
        self.overviewTextView.text = viewModel.schoolOverview
        
        self.noOfSATTesttakersLabel.text = viewModel.numberOfSATTestTakers
        self.mathScoreLabel.text = viewModel.mathAvgScore
        self.readingScoreLabel.text = viewModel.readingAvgScore
        self.writingScoreLabel.text = viewModel.writingAvgScore
    }
}

extension SchoolsDetailViewController {
    
    /// method to show alert, can make it a generic method to accept title and message and much more
    func showAlert() {
        let alertController = UIAlertController.init(title: "Oh Sorry", message: "Can please try in real device, can't run this functionality in simulator", preferredStyle: .alert)
        let okAction = UIAlertAction.init(title: "OK", style: .default)
        alertController.addAction(okAction)
        self.present(alertController, animated: true)
    }
}
