//
//  SchoolSATModel.swift
//  20230515-SandeepRajuVysyaraju-NYCSchools
//
//  Created by SandeepRaju Vysyaraju  on 16/05/23.
//

import Foundation

// MARK: - SchoolSATModel
struct SchoolSATModel: Codable {
    let dbn: String
    let schoolName: String
    let numOfSatTestTakers: String
    let satCriticalReadingAvgScore: String
    let satMathAvgScore: String
    let satWritingAvgScore: String

    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case schoolName = "school_name"
        case numOfSatTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
    }
}
